﻿
using UnityEngine;
using System.Collections;
[RequireComponent(typeof(AudioSource))]

public class PuckControl : MonoBehaviour {
	public AudioClip wallCollideClip;
	public AudioClip paddleCollideClip;
	public LayerMask paddleLayer;
	private AudioSource audio;

	void Start(){
		audio = GetComponent<AudioSource> ();
	}

	void OnCollisionEnter(Collision collision){
		if (paddleLayer.Contains (collision.gameObject)) {
			audio.PlayOneShot (paddleCollideClip);
		} else {
			audio.PlayOneShot (wallCollideClip);
		}
	}
}
