﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(AudioSource))]
public class Goal : MonoBehaviour {
	public AudioClip scoreClip;
	private AudioSource audio;
	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void OnTriggerEnter (Collider collider) {
		audio.PlayOneShot (scoreClip);
	}
}
