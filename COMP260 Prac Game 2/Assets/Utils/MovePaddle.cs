﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(Rigidbody))]
public class MovePaddle : MonoBehaviour {
	private Rigidbody rigidbody;
	void Start () {
		rigidbody = GetComponent<Rigidbody> ();
		rigidbody.useGravity = false;
	}
	private Vector3 GetMousePositon() {
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		Plane plane = new Plane (Vector3.forward, Vector3.zero);
		float distance = 0;
		plane.Raycast (ray, out distance);
		return ray.GetPoint (distance);
	}
	void OnDrawGizmos(){
		Gizmos.color = Color.yellow;
		Vector3 pos = GetMousePositon ();
		Gizmos.DrawLine (Camera.main.transform.position, pos);
	}
	public float speed = 5f;
	void FixedUpdate(){
		Vector3 pos = GetMousePositon ();
		Vector3 dir = pos - rigidbody.position;
		Vector3 vel = dir.normalized * speed;

		float move = speed * Time.fixedDeltaTime;
		float distToTarget = dir.magnitude;

		if (move > distToTarget) {
			vel = vel * distToTarget / move;
		}
		rigidbody.velocity = vel;
	}

}
